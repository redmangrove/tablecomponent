import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { catchError } from 'rxjs/operators';



@Injectable({
	providedIn: 'root'
})
export class SubmitService {

	baseUrl = environment.baseUrl;
	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type':  'application/json',
			Authorization: 'my-token-here'
		})
	};

	constructor( private http: HttpClient ) { }

submitId(req): Observable<any> {
	return this.http.post(this.baseUrl, req, this.httpOptions)
	.pipe(
		catchError( error => this.handleError(error)
	));
}

//  This can go in a different folder or file
// to handle all errors from services
handleError(error: HttpErrorResponse) {
	if (error.error instanceof ErrorEvent) {
		console.error('An error occurred:', error.error.message);
	} else {
		console.error(
			`Backend returned code ${error.status}, `);
	}
	return throwError(
		'Something went wrong; please try again later.');
	}
}
