import { Component, OnInit } from '@angular/core';
import { sampledata } from '../../../mock/sample_data';
// in case of trying a different sample data use the one below
// or any data to test
import { sampledata1 } from '../../../mock/sample_data.1';
// In case a model is needed, this one can be used
import { DataSample } from '../models/model-data';
import { SubmitService } from '../submit.service';

@Component({
	selector: 'app-table',
	templateUrl: './table.component.html',
	styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

	data: any;
	headers: string[];
	keys: string[];

	countValues: number[] = [1, 5, 10, 25, 50];
	count = 5;
	actualPage = 0;
	hasPrev = false;
	hasNext = true;
	// showPagination: any;

	constructor(private submit: SubmitService) { }

	ngOnInit() {

		console.log(sampledata.length);
		console.log(this.actualPage);
		console.log(this.count);

		this.data = this.loadData(this.actualPage);
		this.headersFromKeys(this.data);
	}
	headersFromKeys(data) {
		// assuming the array is uniform
		const ele = sampledata.slice(0, 1);
		this.headers = Object.keys(ele[0]);

	}
	next() {
	 this.actualPage = this.actualPage * this.count + this.count >= sampledata.length ? this.actualPage : this.actualPage + 1;
	 if (sampledata.length / this.count > this.actualPage ) {
			console.log(this.actualPage);
			this.loadData(this.actualPage);
	}
	}

	prev() {
		this.actualPage = this.actualPage === 0 ? 0 : this.actualPage - 1;
		console.log(this.actualPage);
		this.loadData(this.actualPage, );
	}

	onChange() {
		this.actualPage = 0;
		this.loadData(0);
	}

	loadData(page) {
		console.warn(page);
		console.warn();
		this.data =  sampledata.slice((page) * this.count, (page) * this.count + this.count);
		this.hasPrev = true;
		this.hasNext = true;
		return this.data;
	}
	submitId(id, status) {
	window.confirm('Your submition with id ' + id + ' and status ' + status +
		' will be sent. \nPlease review Network tool');
	const postReq = {id, status};
	this.submit.submitId(postReq)
		.subscribe( response => {
			console.log(response);
		});
	}
}
