import { TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { environment } from 'src/environments/environment.prod';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { sampledata } from '../../mock/sample_data';



import { SubmitService } from './submit.service';

describe('SubmitService testing', () => {
	let httpClient: HttpClient;
	let httpTestControl: HttpTestingController;
	let service: SubmitService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [ HttpClientTestingModule],
			providers: [ SubmitService ]
		});
	 service =  TestBed.get(SubmitService);
	 httpClient = TestBed.get(HttpClient);
	 httpTestControl = TestBed.get(HttpTestingController);
	});


	it('baseUrl should be api/submit', () => {
		expect(service.baseUrl).toBe(environment.baseUrl);
	});

	it('make expected call', () => {
		const mockData = {
		id : 1,
		status: 'read'
		};

	 service.submitId(mockData)
	.subscribe( rowSubmitted => {
		expect( rowSubmitted).toEqual(mockData);
			});
		const req = httpTestControl.expectOne(environment.baseUrl, 'calling api');
		expect( req.request.method).toBe('POST');
		req.flush(mockData);
		});
});

